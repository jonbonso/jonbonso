/**
 * Created by jsbonso on 21/03/16.
 */

angular.module('amp').factory("DataService", ['$http', function ($http) {
  'ngInject';
  return {
    getData: function () {
      return $http.get('userData.json');
    }
  }
}]);
