/**
 * Created by jsbonso on 21/03/16.
 */

'use strict';

var amp = angular.module('amp',[]);

angular.element(document).ready(function () {
  angular.bootstrap(document, ['amp'], {
    strictDi: true
  });
});



