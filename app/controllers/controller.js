/**
 * Created by jsbonso on 21/03/16.
 */

'use strict';

angular.module('amp').controller('AmpController', ['$scope', 'DataService', function($scope, dataService){
  'ngInject';

  $scope.searchTerm = '';

  dataService.getData().then(function(response){
    $scope.searchData = response.data;
  });

}]);
