/**
 * Created by jsbonso on 21/03/16.
 */

'use strict';

describe('Amp Code Unit Tests', function() {

  var $httpBackend, $rootScope, $controller, createController, $compile;

  beforeEach(module('amp'));

  beforeEach(inject(function($injector) {
    $httpBackend = $injector.get('$httpBackend');

    $httpBackend.when('GET', 'userData.json')
      .respond(JSON.stringify(data));

    $compile = $injector.get('$compile');
    $rootScope = $injector.get('$rootScope');
    var $controller = $injector.get('$controller');

    createController = function() {
      return $controller('AmpController', {'$scope' : $rootScope });
    };
  }));

  it('should filter items when search text has been changed', function() {
    $rootScope.searchData = data;
    $rootScope.searchTerm = 'c';

    var html = $compile('<search-list items="searchData" search="searchTerm"></search-list>')($rootScope);
    $rootScope.$digest();
    var test = html[0].getElementsByTagName('h4');
    expect(test.length).toEqual(2);
  });

  it('should populate searchData controller', function() {

    $httpBackend.expectGET('userData.json');
    var controller = createController();
    $httpBackend.flush();

    expect($rootScope.searchData.length).toEqual(6);
  });


});
