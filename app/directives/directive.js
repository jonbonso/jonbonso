/**
 * Created by jsbonso on 21/03/16.
 */
//Directive
angular.module('amp').directive('searchList', function () {
  return {
    restrict: 'EA',
    scope: {
      items: '=',
      search: '='
    },
    template: '	<div class="row text-center"> \
        				<div class="col-sm-6 col-md-4" ng-repeat="item in items | filter:{firstName:search} "> \
        					<img ng-src="{{item.picture}}" alt="{{item.firstName}} {{item.lastName}}"> \
        					<h4> {{item.firstName}}</h4> \
        				</div>\
        			</div>'
  }
});
